package com.example.week_3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    Button submit;
     EditText username,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        submit = findViewById(R.id.login);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                if (TextUtils.isEmpty(password.getText()))
                    password.setError("Password is required");
            else
               if (!isValidPassword(password.getText().toString().trim())) {
                   password.setError("Invalid Password pattern");

                }


                if (TextUtils.isEmpty(username.getText())) {
                    username.setError("Username is required");

                }else
                    if (username.getText().toString().equals("F180207")) {
                    Toast.makeText(getApplicationContext(), "Correct Username", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, Home.class);
                    startActivity(intent);
                } else {
                    alert.setTitle("Alert")
                            .setMessage("Invalid Username")
                            .setNegativeButton("Close", null)
                            .show();
                }


            }
        });
    }


            public boolean isValidPassword(final String password) {

                Pattern pattern;
                Matcher matcher;

                final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

                pattern = Pattern.compile(PASSWORD_PATTERN);
                matcher = pattern.matcher(password);

                return matcher.matches();

            }
    public void goToSignup(View view) {
        Intent intent = new Intent(MainActivity.this,Signup.class);
        startActivity(intent);
    }


}
