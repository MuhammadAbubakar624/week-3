package com.example.week_3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Signup extends AppCompatActivity {
    Button submit;
    EditText  firstname ,lastname,email,password,number, address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        submit = findViewById(R.id.signupbtn);
        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        number = findViewById(R.id.number);
        address = findViewById(R.id.address);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (TextUtils.isEmpty(firstname.getText()))
                    firstname.setError("firstname is required");
                if (TextUtils.isEmpty(lastname.getText()))
                    lastname.setError("lastname is required");
                if (TextUtils.isEmpty(email.getText()))
                    email.setError("email is required");
                if (TextUtils.isEmpty(firstname.getText()))
                    firstname.setError("firstname is required");
                if (TextUtils.isEmpty(password.getText()))
                    password.setError("password is required");
                if (TextUtils.isEmpty(address.getText()))
                    address.setError("address is required");
                Intent intent = new Intent(Signup.this, MainActivity.class);
                startActivity(intent);



            }
        });

    }

    public void goToLogin(View view) {
        Intent intent = new Intent(Signup.this,
                MainActivity.class);
        startActivity(intent);
    }
}